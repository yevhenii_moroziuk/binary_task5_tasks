﻿using System;

namespace Client.Models
{
	public class Team
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime? Created_At { get; set; }

		public override string ToString()
		{
			return $"Id: {Id}, Name: {Name}, Created_At: {Created_At}\n";
		}
	}
}
