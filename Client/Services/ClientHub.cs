﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace Client.Services
{
	public class ClientHub
	{
		private HubConnection _hubConnection;

		public ClientHub(IConfiguration configuration)
		{
			//if can`t find configuration
			_hubConnection = new HubConnectionBuilder()
						.WithUrl(configuration.GetSection("HubUrl").Value)//"https://localhost:44305/hubmes")
						.Build();
		}			
		public async Task WorkAsync()
		{
			await _hubConnection.StartAsync();

			_hubConnection.On<string>("SendMessage", (message) => { Console.WriteLine(message); });
		}

		public async Task StopAsync()
		{
			await _hubConnection.StopAsync();
		}
	}
}
