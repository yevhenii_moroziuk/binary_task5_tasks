﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Client.Services
{
	public class DataClient:IDataClient
	{
		private HttpClient _client;
		private readonly string _base;

		public DataClient(IConfiguration configuration)
		{
			_client = new HttpClient();
			//if can`t find configuration
			_base = configuration.GetSection("BaseUrl").Value;//"https://localhost:44305/api/";
		}

		public async Task<T> GetAsync<T>(string specificAddres)
		{
			var ans = await _client.GetStringAsync(_base + specificAddres);
			return JsonConvert.DeserializeObject<T>(ans);
		}

		public async Task EditAsync<T>(T obj, string specificAddres)
		{
			string output = JsonConvert.SerializeObject(obj);

			_client = new HttpClient();

			await _client.PutAsync(_base + specificAddres, new StringContent(output.ToString()));

			//string output = JsonConvert.SerializeObject(obj);

			//HttpRequestMessage request = new HttpRequestMessage()
			//{

			//	Content = new StringContent(output.ToString(),
			//	Encoding.UTF8, "application/json"),
			//	Method = HttpMethod.Put,
			//	RequestUri = new Uri(_base + specificAddres)
			//};
		}
	}
}
