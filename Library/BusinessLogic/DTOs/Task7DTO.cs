﻿using Library.DataAccess.Models;

namespace Library.BusinessLogic.DTOs

{
	public class Task7DTO
	{
		public Project Project { get; set; }
		public Task TaskByDescription { get; set; }
		public Task TaskByLength { get; set; }
		public int CountOfUsers { get; set; }

		public Task7DTO(Project project, Task task1, Task task2, int count)
		{
			Project = project;
			TaskByDescription = task1;
			TaskByLength = task2;
			CountOfUsers = count;
		}
	}
}
