﻿using AutoMapper;
using Library.BusinessLogic.DTOs;
using Library.DataAccess.Models;

namespace Library.Mapping
{
	public class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			//User
			CreateMap<User, UserDTO>().ReverseMap();
			//Team
			CreateMap<Team, TeamDTO>().ReverseMap();
			//Task
			CreateMap<Task, TaskDTO>();
			CreateMap<TaskDTO, Task>();
			//Project
			CreateMap<Project, ProjectDTO>().ReverseMap();
		}
	}
}
