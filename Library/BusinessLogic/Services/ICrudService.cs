﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Services
{
	public interface ICrudService<TEntity>
	{
		Task CreateAsync(TEntity entity);
		Task UpdateAsync(TEntity entity);
		Task DeleteAsync(int id);
		Task<List<TEntity>> ShowAllAsync();
		Task<TEntity> ShowAsync(int id);
		Task<bool> IsExistsAsync(int id);

	}
}
