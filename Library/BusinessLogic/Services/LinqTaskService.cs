﻿using Library.BusinessLogic.DTOs;
using Library.DataAccess;
using Library.DataAccess.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Library.Services
{
	public class LinqTaskService:ILinqTaskService
	{
		private BinaryDataContext _context;
		private readonly IConfiguration _config;

		public LinqTaskService(BinaryDataContext context, IConfiguration configurations)
		{
			_context = context;
			_config = configurations;
		}

		//ex 1
		public async Task<IDictionary<string, int>> GetTasksCountAsync(int author) {

			//var proj = _context.Projects.Where(p => p.Author_Id == author)
			//	.Include(p => p.Tasks).GroupBy( p=>p.Name, p => p.Tasks);

			//return await proj.ToDictionaryAsync(pr=>pr.Key, pr=>pr.Count());
			return await (from project in _context.Projects
						  join tasks in _context.Tasks
						  on project.Id equals tasks.Project_Id
						  where project.Author_Id == author
						  group project.Tasks.Count() by project.Name)
						  //group project.Tasks by project.Name)
				 .ToDictionaryAsync(pr => pr.Key, pr => pr.Count());
		}

		//ex 2
		public async Task<List<DataAccess.Models.Task>> GetTasksAsync(int user_Id) =>
			await _context.Tasks.Where(task => task.Performer_Id == user_Id && task.Name.Length < 45).ToListAsync();

		//ex 3
		public async Task<List<string>> GetFinishedTasksAsync(int user_Id) =>
			await _context.Tasks.Where(task => task.Performer_Id == user_Id && task.Finished_At.Year == 2019)
			.Select(task => $"Task Id: {task.Id}, Task name: {task.Name}")
			.ToListAsync();

		//ex 4
		public async Task<IDictionary<string, List<User>>> GetTeamDefinitionAsync() =>

			await (from task in _context.Teams
			 join user in _context.Users
			 on task.Id equals user.Team_Id
			 where task.Users.All(user => (DateTime.Now.Year - user.Birthday.Year) > 12)
			 group user by task)
			 .ToDictionaryAsync(d => $"Company id: {d.Key.Id}, Company name: {d.Key.Name}", d =>
			 (
				 d.OrderByDescending(u => u.Registered_At).ToList()
			 ));


		//ex 5
		public async Task<IDictionary<User, List<DataAccess.Models.Task>>> GetUserTasksAsync() =>

			await (from task in _context.Tasks
			 join user in _context.Users
			 on task.Performer_Id equals user.Id
			 group task by user)
			 .OrderBy(k => k.Key.First_Name)
			 .ToDictionaryAsync(d => d.Key, d =>
			 (
				 d.OrderByDescending(t => t.Name.Length).ToList()
			 ));

		//ex 6
		public async Task<Task6DTO> GetUserDefinitionAsync(int user_id) =>

			await (from user in _context.Users
			 join project in _context.Projects on user.Id equals project.Author_Id
			 join task in _context.Tasks on project.Id equals task.Project_Id
			 let lastPrId = user.Projects.OrderByDescending(p => p.Created_At).First()//to disable lazy loading
			 where user.Id == user_id
			 select new Task6DTO
			 (
				 user,
				 user.Projects.OrderBy(p => p.Created_At).Last(),
				 //user.Tasks.Where(t => t.Project_Id == lastPrId.Id).Count(),     //if only current user`s tasks
				 _context.Tasks.Where(t => t.Project_Id == lastPrId.Id).Count(),//if all tasks project
				 user.Tasks.Where(t => t.Finished_At > DateTime.Now).Count(),
				 user.Tasks.OrderBy(t => t.Finished_At - t.Created_At).Last()
			 )).FirstOrDefaultAsync();


		//ex 7
		public async Task<Task7DTO> GetProjectDefinitionAsync(int project_id) =>

			 await (from team in _context.Teams
			  join project in _context.Projects
			  on team.Id equals project.Team_Id
			  join task in _context.Tasks
			  on project.Id equals task.Project_Id

			  let targetProject = _context.Projects
					.Where(p => p.Description.Length > 25 || p.Tasks.Count() < 3)
					.SingleOrDefault(p => p.Id == project_id)//check if currentt Project satisfy conditions
			  where project.Id == project_id
			  select new Task7DTO(
				  project,
				  project.Tasks.OrderBy(ts => ts.Description.Length).Last(),
				  project.Tasks.OrderBy(ts => ts.Name.Length).First(),
				  targetProject == null ? 0 : project.Team.Users.Count()//return 0 if do not satisfy
				  )
				).FirstOrDefaultAsync();

		//rabbitmq data 
		public async Task<List<LogModel>> GetLogAsync()
		{
			try
			{
				List<LogModel> temp = new List<LogModel>();
				//temp.Add(new LogModel { Message = "", WrittenData = DateTime.Now });
				using (StreamReader r = File.OpenText(_config.GetSection("FilePath").Value))
				{
					string line;
					while ((line = await r.ReadLineAsync()) != null)
					{
						temp.Add(JsonConvert.DeserializeObject<LogModel>(line));
					}
				};
				return temp.OrderByDescending(m => m.WrittenData).ToList();
			}
			catch(Exception e)
			{
				//return null;
				throw new Exception(e.Message);
			}
		}
	}
}
