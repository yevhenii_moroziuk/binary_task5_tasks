﻿using AutoMapper;
using Library.BusinessLogic.DTOs;
using Library.DataAccess;
using Library.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Library.Services
{
	public class TeamService:ICrudService<TeamDTO>
	{
		private readonly BinaryDataContext _context;
		private readonly IMapper _mapper;

		public TeamService(BinaryDataContext source, IMapper mapper)
		{
			_context = source;
			_mapper = mapper;
		}

		public async Task CreateAsync(TeamDTO entity)
		{
			await _context.Teams.AddAsync(_mapper.Map<Team>(entity));

			await _context.SaveChangesAsync();
		}

		public async Task DeleteAsync(int id)
		{
			var team = await _context.Teams.SingleOrDefaultAsync(u => u.Id == id);
			_context.Teams.Remove(team);

			await _context.SaveChangesAsync();
		}

		public async Task<TeamDTO> ShowAsync(int Id)
		{
			var team = await _context.Teams.SingleOrDefaultAsync(u => u.Id == Id);
			return _mapper.Map<TeamDTO>(team);
		}

		public async Task<List<TeamDTO>> ShowAllAsync()
		{
			var teams = await _context.Teams.ToListAsync();
			return _mapper.Map<List<TeamDTO>>(teams);
		}

		public async Task UpdateAsync(TeamDTO entity)
		{
			var team = await _context.Teams.SingleOrDefaultAsync(u => u.Id == entity.Id);//find this user in collection
			team = _mapper.Map<Team>(entity);

			await _context.SaveChangesAsync();
		}

		public async Task<bool> IsExistsAsync(int id)
		{
			return await _context.Teams.SingleOrDefaultAsync(u => u.Id == id) == null ? false : true;
		}
	}
}
