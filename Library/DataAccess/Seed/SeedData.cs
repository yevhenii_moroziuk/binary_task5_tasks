﻿using Library.DataAccess.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Library.DataAccess.Seed
{
	public class SeedData
	{
		public static List<T> Seed<T>(string filename)
		{
			var parsedData = System.IO.File.ReadAllText($"../Library/DataAccess/Seed/_{filename}.json");
			return JsonConvert.DeserializeObject<List<T>>(parsedData);
		}
	}

	public class SeedService
	{
		private readonly BinaryDataContext _context;

		public SeedService(BinaryDataContext context)
		{
			_context = context;
		}

		public void SeedEntities()
		{
			var users = SeedData.Seed<User>("users");
			var projects = SeedData.Seed<Project>("projects");
			var tasks = SeedData.Seed<Task>("tasks");
			var teams = SeedData.Seed<Team>("teams");

			foreach (var item in teams)
			{
				_context.Teams.Add(item);
			}
			foreach (var item in users)
			{
				_context.Users.Add(item);
			}
			foreach (var item in projects)
			{
				_context.Projects.Add(item);
			}
			foreach (var item in tasks)
			{
				_context.Tasks.Add(item);
			}
			

			_context.SaveChanges();
		}
	}
}
