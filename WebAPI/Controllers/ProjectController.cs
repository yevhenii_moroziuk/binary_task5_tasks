﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Library.BusinessLogic.DTOs;
using Library.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
	[Route("api/projects")]
	[ApiController]
	public class ProjectController : ControllerBase
	{
		private readonly ICrudService<ProjectDTO> _service;

		public ProjectController(ICrudService<ProjectDTO> service)
		{
			_service = service;
		}

		[HttpGet]
		public async Task<ActionResult<List<ProjectDTO>>> GetProjectsAsync()
		{
			return await _service.ShowAllAsync();
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetProjectAsync(int id)
		{
			if (!await _service.IsExistsAsync(id))
				return NotFound();

			return Ok(await _service.ShowAsync(id));
		}

		[HttpPost]
		public async Task<IActionResult> PostProjectAsync(ProjectDTO projectDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (await _service.IsExistsAsync(projectDTO.Id))
				return BadRequest();

			try
			{
				await _service.CreateAsync(projectDTO);
				return Created("api/projects", projectDTO);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid data");
			}
		}

		[HttpPut]
		public async Task<IActionResult> EditProjectAsync(ProjectDTO entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid data");

			if (!await _service.IsExistsAsync(entity.Id))
				return NotFound();

			try
			{
				await _service.UpdateAsync(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("Invalid data");
			}
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteProjectsAsync(int id)
		{
			if (!await _service.IsExistsAsync(id))
				return BadRequest();

			await _service.DeleteAsync(id);

			return Ok("deleted");
		}
	}
}