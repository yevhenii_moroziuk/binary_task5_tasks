﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Library.BusinessLogic.DTOs;
using Library.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
	[Route("api/teams")]
    [ApiController]
    public class TeamController : ControllerBase
	{
		private readonly ICrudService<TeamDTO> _service;

		public TeamController(ICrudService<TeamDTO> service)
		{
			_service = service;
		}

		[HttpGet]
		public async Task<ActionResult<List<TeamDTO>>> GetTeamsAsync()
		{
			return await _service.ShowAllAsync();
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetTeamAsync(int id)
		{
			if (!await _service.IsExistsAsync(id))
				return NotFound();

			return Ok(await _service.ShowAsync(id));
		}

		[HttpPost]
		public async Task<IActionResult> PostTeamAsync(TeamDTO teamDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (await _service.IsExistsAsync(teamDTO.Id))
				return BadRequest();

			try
			{
				await _service.CreateAsync(teamDTO);
				return Created("api/teams", teamDTO);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid data");
			}
		}

		[HttpPut]
		public async Task<IActionResult> EditTeamAsync(TeamDTO entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid data");

			if (!await _service.IsExistsAsync(entity.Id))
				return NotFound();

			try
			{
				await _service.UpdateAsync(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("Invalid data");
			}
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteTeamAsync(int id)
		{
			if (!await _service.IsExistsAsync(id))
				return BadRequest();

			await _service.DeleteAsync(id);

			return Ok("deleted");
		}
	}
}