﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using WebAPI.Hubs;

namespace WebAPI.Queue
{
	public class QueueService
	{
		private IHubContext<MessageHub> _hub;
		private readonly IConfiguration _config;
		private IConnection _connection;
		private readonly IModel _channel;
		private readonly EventingBasicConsumer _consumer;

		public QueueService(IConfiguration configuration, IHubContext<MessageHub> hub)
		{
			_config = configuration;
			/////////////////////
			_hub = hub;
			/////////////////////
			var factory = new ConnectionFactory()
			{
				Uri = new Uri(_config.GetSection("Rabbit").Value)
			};

			_connection = factory.CreateConnection();
			_channel = _connection.CreateModel();
			_channel.ExchangeDeclare("SecondExchange", ExchangeType.Direct);

			_channel.QueueDeclare(queue: "dataFromWorker",
								 durable: false,
								 exclusive: false,
								 autoDelete: false,
								 arguments: null);

			_channel.QueueBind("dataFromWorker", "SecondExchange", "hello");

			_consumer = new EventingBasicConsumer(_channel);

			_consumer.Received += _consumer_Received;

			_channel.BasicConsume("dataFromWorker", false, _consumer);
			////////////////////
		}

		private void _consumer_Received(object sender, BasicDeliverEventArgs e)
		{
			//тут будет хаб
			var body = e.Body;
			var message = Encoding.UTF8.GetString(body);

			_hub.Clients.All.SendAsync("SendMessage", message);//.Wait();
			_channel.BasicAck(e.DeliveryTag, false);
		}

		public void TransferMessage(string message)
		{
			var factory = new ConnectionFactory()
			{
				Uri = new Uri(_config.GetSection("Rabbit").Value)
			};

			using (var connection = factory.CreateConnection())
			{
				using (var channel = connection.CreateModel())
				{

					channel.ExchangeDeclare("MainExchange", ExchangeType.Direct);

					var body = Encoding.UTF8.GetBytes(message);

					channel.BasicPublish(exchange: "MainExchange",
										 routingKey: "hello",
										 basicProperties: null,
										 body: body);
				}
			}
		}
	}
}
