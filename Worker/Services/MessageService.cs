﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Task4.Worker.Models;

namespace Task4.Worker.Services
{
	class MessageService:IDisposable
	{
		private IConnection _connection;
		private IModel  _channel;
		private EventingBasicConsumer _consumer;

		public MessageService()
		{

		}

		public void Run()
		{
			Console.WriteLine("Service run");
			Configure();
		}

		private void Configure()
		{
			var factory = new ConnectionFactory()
			{
				Uri = new Uri("amqp://admin:admin@localhost:5672"/*_config.GetSection("Rabbit").Value*/)
			};

			_connection = factory.CreateConnection();
			_channel = _connection.CreateModel();
			_channel.ExchangeDeclare("MainExchange", ExchangeType.Direct);

			_channel.QueueDeclare(queue: "dataFromServer",
								 durable: false,
								 exclusive: false,
								 autoDelete: false,
								 arguments: null);

			_channel.QueueBind("dataFromServer", "MainExchange", "hello");

			_consumer = new EventingBasicConsumer(_channel);

			_consumer.Received += _consumer_ReceivedAsync;

			_channel.BasicConsume("dataFromServer", false, _consumer);
		}

		private void SendStatus(string message)
		{
			var factory = new ConnectionFactory()
			{
				Uri = new Uri("amqp://admin:admin@localhost:5672"/*_config.GetSection("Rabbit").Value*/)
			};

			using (var connection = factory.CreateConnection())
			{
				using (var channel = connection.CreateModel())
				{

					channel.ExchangeDeclare("SecondExchange", ExchangeType.Direct);

					var body = Encoding.UTF8.GetBytes(message);

					channel.BasicPublish(exchange: "SecondExchange",
										 routingKey: "hello",
										 basicProperties: null,
										 body: body);
				}
			}
		}

		private async void _consumer_ReceivedAsync(object sender, BasicDeliverEventArgs e)
		{
			var body = e.Body;
			var message = Encoding.UTF8.GetString(body);
			// write to file
			try
			{
				string json = JsonConvert.SerializeObject(new LogModel
				{
					WrittenData = DateTime.Now,
					Message = message
				});

				using (var w = new FileStream("../../../../DataLog.txt", FileMode.OpenOrCreate))
				{
					w.Seek(0, SeekOrigin.End);
					await w.WriteAsync(Encoding.Default.GetBytes(json));
					await w.WriteAsync(Encoding.ASCII.GetBytes(Environment.NewLine));
				}

			}
			catch
			{
				message = "can`t save data to file";
			}

			SendStatus($"handled {message} and sent to server");

			Console.WriteLine(message);
			Console.WriteLine("message handled");
			_channel.BasicAck(e.DeliveryTag, false);
		}

		public void Dispose()
		{
			_connection?.Dispose();
			_channel?.Dispose();
		}
	}
}
